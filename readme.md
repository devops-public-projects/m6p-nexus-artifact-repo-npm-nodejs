# Artifact Repository Manager with Nexus
For this project, I utilized NPM to package and deploy a NodeJS application to Nexus Artifact Repository Manager running on a remote DigitalOcean droplet server. 

I then wrote a bash script to query the Nexus Rest API to get the latest artifact version information to download it. Finally the script runs the application as the service account user for node applications.

## Technologies Used
- DigitalOcean Droplets
- Linux (Ubuntu)
- Sonatype Nexus Artifact Repository Manager
- Java
- NPM
- NodeJS
- Bash

## Project Description
- Build NodeJS application locally
- Upload NodeJS application to Nexus via NPM Deploy
- Query Nexus Rest API for artifact information
- Download artifact
- Run artifact on another Droplet server with a node-svc user

## Prerequisites
- 2 Droplet servers
	- 1 that hosts the Nexus Artifact Repository
	- 1 that is configured with a node-svc user to run NodeJS applications
- NodeJS server has NPM and NodeJS installed as well as any other dependencies

## Steps Taken
### Remote Server Steps
### Prep Nexus Repository
- Create a new blob store to hold the new repository
- Create a new repository (`npm-snapshots`) and utilize the newly made blob store
- Create a new role that has `nx-repository-view-npm-*-*` access
- Create a new user that has the above role as a privilege
	- We'll use this user to download the Artifact from Nexus later

---

### Local Server Steps
### Download and Build Package
- `mkdir ~/code_repo`
- `cd ~/code_repo`
- `git clone https://gitlab.com/twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises.git`
- `cd cloud-basics-exercises`

### Deploy Package to Nexus
- `npm package app`
- `npm publish --registry=http://REMOTE_SERVER_IP:8081/repository/npm-snapshots/ bootcamp-node-project-1.0.0.tgz`
	- Replace the `REMOTE_SERVER_IP` with your Nexus server IP

After these steps, your artifact is now available in the repository.

![Successful Upload](/images/m6p-1-successful-upload.png)

### Nexus Rest API Query to Download and Run Artifact
- Using the provided bash script `` we can then query the Rest API, find the latest version, download that version and run the application.
- `su node-svc`
- `cd ~/scripts`
- `./download_and_run`

The application will now be running and listening on port 3000

![The NodeJS application is now running](/images/m6p-2-application-running.png)

